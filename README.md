# Super Slider
# 2 axis synchronized camera slider with menu based on ATmega328

## Building project in Arduino IDE

### Configure libraries

Make sure that following libraries are installed first: 

- U8g2 by oliver
- ClickEncoder from https://github.com/soligen2010/encoder

### Compile project

- Open firmware.ino sketch in firmware subdir
- Optional: open file firmware/src/settings.hpp and change settings if necessary
- compile and upload

# Двух осевой синхронный слайдер камеры на чипе ATmega328 (Arduino nano uno итд)

## Как скомпилировать проект в Arduino IDE

### Библиотеки

Убедитесь, что следуюшие библиотеки установлены: 

- U8g2 by oliver
- ClickEncoder from https://github.com/soligen2010/encoder

### Компилирование

- Откройте скетч firmware.ino в поддериктории firmware
- Опционально: откройте и отредактируйте файл firmware/src/settings.hpp если необходимо
- скомпилируйте и загрузите

### Параметры файла firmware/src/settings.hpp


| Параметр | Значение | Начальное
| ------ | ------ | ------ |
| smallMenuText   | размер меню (не реализовано)    | false |
| screenWidth     | ширина OLED экрана              | 128   |
| screenHeight    | высота OLED экрана              | 64    |
| encoderCLK      | пин энкодера CLK                | 3     |
| encoderDT       | пин энкодера DT                 | 8     |
| encoderSW       | пин энкодера SW                 | 2     |
| encoderSTEPS    | количество шагов энкодера       | 4     |
| encoderMenuINV  | инвертирование направления меню | false |
| encoderAccel    | включение ускорения энкодера    | false |
| stepperEnable   | пин включения шагового двигателя X | 9  |
| stepperX_SPerMM | количество шагов на мм по оси X    | 80 |
| stepperX_INV    | инверсия направления X             | false |
| stepperX_DIR    | пин направления шагового двигателя X | 4 |
| stepperX_STEP   | пин шага шагового двигателя X        | 5 |
| xlimitEndstopP  | пин концевого выключателя X          | 11 |
| xlimitEndstopL  | уровень сигнала концевого выключателя X при срабатывании  | false |
| xLimit          | размер оси X в мм                    | 1500 |
| xSpeedLimit     | ограничесние скорости проезда оси X в 0.1 мм/с | 2000 |
| xSpeedPos       | скорость позиционирования оси X в 0.1 мм/с | 1000 |
| xSpeedHoming    | скорость поиска концевого выключателя оси X в 0.1 мм/с | 400 |
| fineXmovement   | дельта перемещения по оси X при тонкой настройке в мм | 10 |
| coarseXmovement | дельта перемещения по оси X при грубой настройке в мм | 50 |
| stepperY_SPerREV | количество шагов на полный оборот по оси Y | 12000 |
| stepperY_INV     | инверсия направления Y             | true |
| stepperY_DIR     | пин направления шагового двигателя Y | 6 |
| stepperY_STEP    | пин шага шагового двигателя Y        | 7 |
| ySpeedLimit      | ограничесние скорости проезда оси Y в градус/с | 45 |
| ySpeedPos        | скорость позиционирования оси Y в градус/с | 20 |
| fineYmovement    | дельта перемещения по оси Y при тонкой настройке в градусах | 1 |
| coarseYmovement  | дельта перемещения по оси Y при грубой настройке в градусах | 10 |
