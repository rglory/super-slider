#include "settings.hpp"
#include "strings.hpp"
#include "commands.hpp"
#include "moves.hpp"
#include "menu.hpp"
#include "display.hpp"
#include <EEPROM.h>

Path   currentPath;
Point *cpoint;
bool   fineMovement = false;
bool   movingX      = true;

void setCurrentPoint( bool start )
{
    cpoint = start ? &currentPath.m_begin : &currentPath.m_end;
}

void updateDisplayPos()
{
    displayData.m_state = dsPosition;
    auto &data       = displayData.m_data.m_pos;
    data.m_fine      = fineMovement;
    data.m_posX      = movingX;
    data.m_targetPos = movingX ? targetX() : targetY();
}

void setPointX( bool start )
{
    setCurrentPoint( start );
    fineMovement = false;
    movingX      = true;
    rotateAction = [] ( int d ) {
        moveXTo( d * ( fineMovement ? fineXmovement : coarseXmovement ), xSpeedPos / 2, true );
    };
    clickAction   = []( int b ) { 
        if( b != btnSingleClick ) {
            if( coarseXmovement != fineXmovement )
                fineMovement = !fineMovement;
            return;
        }
        cpoint->x = targetX(); 
        activateMenu(); 
    };
    displayUpdate = updateDisplayPos;
}

void rotateYAction( int d )
{
    moveYTo( d * ( fineMovement ? fineYmovement : coarseYmovement ), ySpeedPos, true );
}

void setPointY( bool start )
{
    setCurrentPoint( start );
    fineMovement = false;
    movingX      = false;
    rotateAction = rotateYAction;
    clickAction  = []( int b ) {
        if( b != btnSingleClick ) {
            if( coarseYmovement != fineYmovement )
                fineMovement = !fineMovement;
            return;
        }
        cpoint->y = targetY(); 
        activateMenu();
    };
    displayUpdate = updateDisplayPos;
}

void homeY()
{
    fineMovement = false;
    movingX      = false;
    rotateAction = rotateYAction;
    clickAction  = []( int b ) {
        if( b != btnSingleClick ) {
            if( coarseYmovement != fineYmovement )
                fineMovement = !fineMovement;
            return;
        }
        zeroY();
        activateMenu();
    };
    displayUpdate = updateDisplayPos;
}

void setSpeedTime()
{
    fineMovement = false;
    rotateAction = [] ( int d ) { 
        auto delta = fineMovement ? 1 : 10;
#ifndef SLOW_MOTION        
        const int reverse = 1;
#else        
        const int reverse = -1;
#endif

        while( d * reverse > 0 and currentPath.m_speed + delta <= xSpeedLimit ) {
            d -= reverse;
            currentPath.m_speed += delta;
        }
        while( d * reverse < 0 and currentPath.m_speed - delta > 0 ) {
            d += reverse;
            currentPath.m_speed -= delta;
        }
    };
    clickAction   = []( int b ) { 
        if( b != btnSingleClick ) {
            fineMovement = !fineMovement;
            return;
        }
        activateMenu(); 
    };
    displayUpdate = [] {
        displayData.m_state = dsSpeedTime;
        auto &data = displayData.m_data.m_speed;

        data.m_fine  = fineMovement;
        data.m_speed = currentPath.m_speed;

        auto distx = abs( currentPath.m_begin.x - currentPath.m_end.x );
        auto disty = abs( currentPath.m_begin.y - currentPath.m_end.y );
        data.m_dist = distx > disty ? distx : disty;
    };
}

void setLoops()
{
    rotateAction = [] ( int d ) { 
        while( d > 0 ) {
            switch( currentPath.m_loops ) {
                case 0        : return;
                case maxLoops : currentPath.m_loops = 0; break;
                default       : ++currentPath.m_loops;   break;
            }
            --d;
        }
        while( d < 0 ) {
            switch( currentPath.m_loops ) {
                case 1        : return;
                case 0        : currentPath.m_loops = maxLoops; break;
                default       : --currentPath.m_loops;          break;
            }
            ++d;
        }
    };
    clickAction   = []( int ) { activateMenu(); };
    displayUpdate = [] {
        displayData.m_state = dsLoops;
        auto &data   = displayData.m_data.m_loops;
        data.m_loops = currentPath.m_loops;
    };
}

void setDelay()
{
    rotateAction = [] ( int d ) { 
        while( d > 0 and currentPath.m_delay < 10 ) {
            --d;
            ++currentPath.m_delay;
        }
        while( d < 0 and currentPath.m_delay > 0 ) {
            ++d;
            --currentPath.m_delay;
        }
    };
    clickAction   = []( int ) { activateMenu(); };
    displayUpdate = []{
        displayData.m_state = dsDelay;
        auto &data   = displayData.m_data.m_delay;
        data.m_delay = currentPath.m_delay;

    };
}


uint8_t  loops        = 0;
uint8_t  loopsDelayed = 0;
uint32_t delayLoop    = 0;
bool     loopToStart;

void updateExecute()
{
    displayData.m_state = dsExecution;

    auto &data   = displayData.m_data.m_exec;

#ifndef AXIS_X_DISABLED
    data.m_currentX = currentX();
    data.m_targetX  = targetX();
#endif
#ifndef AXIS_Y_DISABLED
    data.m_currentY = currentY();
    data.m_targetY  = targetY();
#endif
    data.m_delaying = delayLoop ? ( delayLoop - millis() ) / 1000 : 0;
}

void pauseAction( int )
{
    movedAction = nullptr;
    delayAction = nullptr;
    delayLoop   = 0;
    stopMovement(); 
    activateMenu( mtPauseMenu, true );
}

void loopMovedAction();

void resumeAction()
{
    clickAction = pauseAction; 
    displayUpdate = updateExecute; 
    --loops;
    loopToStart = !loopToStart;
    movedAction  = loopMovedAction;
}

void loopMovedAction()
{
    if( currentPath.m_loops and loops / 2 == currentPath.m_loops ) {
        movedAction = nullptr;
        activateMenu();
        return;
    }
    if( loops > loopsDelayed and currentPath.m_delay ) {
        movedAction  = nullptr;
        loopsDelayed = loops;
        delayLoop    = millis() + currentPath.m_delay * 1000L;
        delayAction  = []{
            if( delayLoop <= millis() ) {
                movedAction  = loopMovedAction;
                delayAction  = nullptr;
                delayLoop    = 0;
            }
        };
        return;
    }
    ++loops;
    const auto &point = loopToStart ? currentPath.m_begin : currentPath.m_end;
    moveTo( point.x, point.y, currentPath.m_speed, true );
    loopToStart = !loopToStart;
}

void executeLoop()
{
    loops         = 0;
    loopsDelayed  = 0;
    loopToStart   = abs( targetX() - currentPath.m_begin.x ) > abs( targetX() - currentPath.m_end.x );
    rotateAction  = nullptr;
    clickAction   = pauseAction;
    movedAction   = loopMovedAction;
    displayUpdate = updateExecute;
}

void gotoPos( bool start )
{
    if( start ) 
        moveTo( currentPath.m_begin.x, currentPath.m_begin.y, xSpeedPos );
    else 
        moveTo( currentPath.m_end.x, currentPath.m_end.y, xSpeedPos );
    rotateAction  = nullptr;
    clickAction   = nullptr;
    movedAction   = [] { movedAction = nullptr; activateMenu(); };
    displayUpdate = updateExecute;
}

bool actionSave   = true;

void saveLoadChoice( bool save )
{
    actionSave = save;
    Menu *saveLoadM = getSaveLoadMenu( save );
    saveLoadM->reset();
    saveLoadM->next();
    activateMenu( save ? mtSaveMenu : mtLoadMenu );
}

void updateSaveLoad()
{
    displayData.m_state = dsSaveLoad;

    auto &data   = displayData.m_data.m_saveLoad;
    data.m_save  = actionSave;
    data.m_delay = currentPath.m_delay;
    data.m_speed = currentPath.m_speed;
    data.m_loops = currentPath.m_loops;

#ifndef AXIS_X_DISABLED
    data.m_begX = currentPath.m_begin.x;
    data.m_endX = currentPath.m_end.x;
#endif
#ifndef AXIS_Y_DISABLED
    data.m_begY = currentPath.m_begin.y;
    data.m_endY = currentPath.m_end.y;
#endif
}

void saveLoadPath( uint8_t num )
{
    actionSave ? savePath( num ) : loadPath( num );
}

void savePath( uint8_t num )
{
    auto p = reinterpret_cast<const uint8_t *>( &currentPath );
    const auto base = num * sizeof( currentPath );
    for( uint8_t i = 0; i < sizeof( currentPath ); ++i ) {
        EEPROM.write( base + i, p[i] );
    }
    displayUpdate = updateSaveLoad;
    clickAction   = [](int) { activateMenu(); };
}

void loadPath( uint8_t num )
{
    auto p = reinterpret_cast<uint8_t *>( &currentPath );
    const auto base = num * sizeof( currentPath );
    for( uint8_t i = 0; i < sizeof( currentPath ); ++i ) {
        p[i] = EEPROM.read( base + i );
    }
    if( currentPath.m_loops == 0xFF ) // EEPROM was not saved before
        currentPath = Path();

    if( currentPath.m_delay == 0xFF )
        currentPath.m_delay = 0;
    
    displayUpdate = updateSaveLoad;
    clickAction   = [](int) { activateMenu(); };
}
