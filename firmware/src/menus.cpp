#include "settings.hpp"
#include "menu.hpp"
#include "strings.hpp"
#include "commands.hpp"

#ifndef REMOTE_CONTROLLER
#define ACTION(x) x
#else
#define ACTION(x) nullptr
#endif

Menu *getMainMenu()
{
    static MenuItem itemsMain[] = {
    #ifndef AXIS_X_DISABLED
        { strHomeX,     ACTION( homeX ) },
    #endif
    #ifndef AXIS_Y_DISABLED
        { strHomeY,     ACTION( homeY ) },
    #endif
    #ifndef AXIS_X_DISABLED
        { strStartX,    ACTION( [] { setPointX( true ); } ) },
    #endif
    #ifndef AXIS_Y_DISABLED
        { strStartY,    ACTION( [] { setPointY( true ); } ) },
    #endif
    #ifndef AXIS_X_DISABLED
        { strEndX,      ACTION( [] { setPointX( false ); } ) },
    #endif
    #ifndef AXIS_Y_DISABLED
        { strEndY,      ACTION( [] { setPointY( false ); } ) },
    #endif
        { strGotoStart, ACTION( [] { gotoPos( true ); } ) },
        { strGotoEnd,   ACTION( [] { gotoPos( false ); } ) },
    #ifndef SLOW_MOTION        
        { strSpeed,     ACTION( setSpeedTime ) },
    #else
        { strTime,     ACTION( setSpeedTime ) },
    #endif
        { strLoops,     ACTION( setLoops ) },
        { strDelay,     ACTION( setDelay ) },
        { strAction,    ACTION( executeLoop ) },
        { strSave,      ACTION( [] { saveLoadChoice( true ); } ) },
        { strLoad,      ACTION( [] { saveLoadChoice( false ); } ) } 
    };

    static Menu mainM( itemsMain, strMainMenu );
    return &mainM;
}

Menu *getSaveLoadMenu( bool save )
{
    static const char i1[] PROGMEM = "1";
    static const char i2[] PROGMEM = "2";
    static const char i3[] PROGMEM = "3";
    static const char i4[] PROGMEM = "4";

    static MenuItem itemsSaveLoad[] = {
        { strCancel, ACTION( []{ activateMenu(); } ) },
        { i1,  ACTION( []{ saveLoadPath( 0 );  } ) },
        { i2,  ACTION( []{ saveLoadPath( 1 );  } ) },
        { i3,  ACTION( []{ saveLoadPath( 2 );  } ) },
        { i4,  ACTION( []{ saveLoadPath( 3 );  } ) },
    };

    static Menu saveLoadM( itemsSaveLoad );
    saveLoadM.setHeader( save ? strSave : strLoad );
    return &saveLoadM;
}

Menu *getPauseMenu()
{
    static MenuItem itemsPause[] = {
        { strContinue,   ACTION( resumeAction ) },
        { strStopAction, ACTION( [] { activateMenu(); } ) }
    };

    static Menu pauseM( itemsPause, strPause );
    return &pauseM;
}