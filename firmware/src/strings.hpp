#ifndef STRINGS_HPP
#define STRINGS_HPP

//#define LANGUAGE_EN
#define LANGUAGE_RU

#if defined( LANGUAGE_EN )
const char strNoConn[]     PROGMEM = "No Connection...";
const char strCorrupted[]  PROGMEM = "Corrupted Data";
const char strMainMenu[]   PROGMEM = "main menu";
const char strHomeX[]      PROGMEM = "home X";
const char strHomeY[]      PROGMEM = "home Y";
const char strStartX[]     PROGMEM = "start X";
const char strStartY[]     PROGMEM = "start Y";
const char strEndX[]       PROGMEM = "end X";
const char strEndY[]       PROGMEM = "end Y";
const char strGotoStart[]  PROGMEM = "goto start";
const char strGotoEnd[]    PROGMEM = "goto end";
const char strSpeed[]      PROGMEM = "speed";
const char strTime []      PROGMEM = "time";
const char strLoops[]      PROGMEM = "loops";
const char strDelay[]      PROGMEM = "delay";
const char strAction[]     PROGMEM = "action";
const char strSave[]       PROGMEM = "save";
const char strLoad[]       PROGMEM = "load";
const char strContinue[]   PROGMEM = "continue";
const char strStopAction[] PROGMEM = "stop action";
const char strMMS[]        PROGMEM = "mm/s";
const char strSEC[]        PROGMEM = "sec";
const char strMIN[]        PROGMEM = "min";
const char strINF[]        PROGMEM = "INF";
const char strMoving[]     PROGMEM = "moving...";
const char strDelaying[]   PROGMEM = "delaying...";
const char strPause[]      PROGMEM = "pause";
const char strSavedPath[]  PROGMEM = "saved path";
const char strLoadedPath[] PROGMEM = "loaded path";
const char strFrom[]       PROGMEM = "fr ";
const char strTo[]         PROGMEM = "to ";
const char strSpeedSRT[]   PROGMEM = "sp ";
const char strLoopsSRT[]   PROGMEM = " lps ";
const char strDelaySRT[]   PROGMEM = " dl ";
const char strSetX[]       PROGMEM = "set X";
const char strSetY[]       PROGMEM = "set Y";
const char strFine[]       PROGMEM = " fine";
const char strHoming[]     PROGMEM = "homing";
const char strCancel[]     PROGMEM = "cancel";

#elif defined( LANGUAGE_RU )
const char strNoConn[]     PROGMEM = "��� �������...";
const char strCorrupted[]  PROGMEM = "����� � ������";
const char strMainMenu[]   PROGMEM = "����";
const char strHomeX[]      PROGMEM = "����� X";
const char strHomeY[]      PROGMEM = "����� Y";
const char strStartX[]     PROGMEM = "��� X";
const char strStartY[]     PROGMEM = "��� Y";
const char strEndX[]       PROGMEM = "��� X";
const char strEndY[]       PROGMEM = "��� Y";
const char strGotoStart[]  PROGMEM = "� ������";
const char strGotoEnd[]    PROGMEM = "� �����";
const char strSpeed[]      PROGMEM = "��������";
const char strTime[]       PROGMEM = "�����";
const char strLoops[]      PROGMEM = "������";
const char strDelay[]      PROGMEM = "��������";
const char strAction[]     PROGMEM = "�����";
const char strSave[]       PROGMEM = "���������";
const char strLoad[]       PROGMEM = "���������";
const char strContinue[]   PROGMEM = "����������";
const char strStopAction[] PROGMEM = "��������";
const char strMMS[]        PROGMEM = "��/�";
const char strSEC[]        PROGMEM = "���";
const char strMIN[]        PROGMEM = "���";
const char strINF[]        PROGMEM = "����";
const char strMoving[]     PROGMEM = "��������...";
const char strDelaying[]   PROGMEM = "��������...";
const char strPause[]      PROGMEM = "�����";
const char strSavedPath[]  PROGMEM = "���� ����";
const char strLoadedPath[] PROGMEM = "���� ����";
const char strFrom[]       PROGMEM = "�� ";
const char strTo[]         PROGMEM = "� ";
const char strSpeedSRT[]   PROGMEM = "��� ";
const char strLoopsSRT[]   PROGMEM = " ��� ";
const char strDelaySRT[]   PROGMEM = " �� ";
const char strSetX[]       PROGMEM = "��� X";
const char strSetY[]       PROGMEM = "��� Y";
const char strFine[]       PROGMEM = " ���";
const char strHoming[]     PROGMEM = "�����";
const char strCancel[]     PROGMEM = "������";

#else
#error "undefined language"
#endif


#endif // STRINGS_HPP
