#ifndef MENU_HPP
#define MENU_HPP

#include "settings.hpp"

struct MenuItem {
    using Action = void(*)();

    const char *m_str;
    Action      m_action;

    MenuItem( const char *str, const Action &ac = nullptr ) : m_str( str ), m_action( ac ) {}
};


struct U8G2;

class Menu {
public:
    template<size_t N>
    Menu( MenuItem (&items)[N], const char *head = nullptr  ) : m_header( head ), m_items( items ), m_size( N ) 
    {
    }

    void setHeader( const char *head ) { m_header = head; }

    // executes action attached to selected menu item if any
    bool act(); 

    // activates next or previous item based on flag
    bool move( bool down ) 
    {
        return down ? next() : prev(); 
    }

    // activates previous, returns true if changed
    bool prev();

    // activates next, returns true if changed
    bool next();

    // reset to the first item
    void reset() { m_selected = 0; }

    void print( U8G2 *display ) const;

    uint8_t         m_selected = 0;
private:
    const char     *m_header;    
    const MenuItem *m_items;;
    uint8_t         m_size;
};



#endif // MENU_HPP