#ifndef MOVES_HPP
#define MOVES_HPP

// check if target reached
bool targetReached();

// initialize movement
void setupMoves();

// Home X until endpoint triggers
void homeX();

// set current X to 0
void zeroX();

// set current Y to 0
void zeroY();

// stop any movement
void stopMovement();

// current X coord
int32_t currentX();

// target X coord
int32_t targetX();

// current Y coord
int32_t currentY();

// current Y coord
int32_t targetY();

// Generate move values for position x mm and speed s mm/s
void moveXTo( int32_t x, uint16_t s, bool delta );

// Generate move values for position y degree and speed s deg/s
void moveYTo( int32_t y, uint16_t s, bool delta );

// Generate move values for position x mm y degree and speed s mm/s for x
void moveTo( int32_t x, int32_t y, uint16_t s, bool fromProgram = false );

#endif // MOVES_HPP

