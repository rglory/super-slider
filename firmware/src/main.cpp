#include <Arduino.h>

#include <ClickEncoder.h>
#include <RF24.h>

#include "menu.hpp"
#include "strings.hpp"
#include "commands.hpp"
#include "moves.hpp"
#include "display.hpp"

#ifdef ENCODER_ENABLED
ClickEncoder encoder( encoderCLK, encoderDT, encoderSW, encoderSTEPS );
//RF24 radio( 1, 2 );

// timer2 1ms
ISR( TIMER2_COMPA_vect )
{
    encoder.service();
}
#endif

EncoderAction rotateAction;
EncoderAction clickAction;
DisplayUpdate displayUpdate;
MovedAction   movedAction;
DelayAction   delayAction;

void setup() 
{
    Serial.begin( 19200 );

#ifdef ENCODER_ENABLED
    encoder.setAccelerationEnabled( encoderAccel );

    // set timer2 to 1ms for encoder
    cli();
    TCCR2A = 0;
    // CS22 - 64 prescaler
    TCCR2B = (1 << CS22) | (1 << WGM22);
    TCNT2  = 0;
    // F_CPU / ( prescaler * F) - 1
    OCR2A  = 249;
    TIMSK2 |= (1 << OCIE2A);
    sei();
#endif

    setupDisplay();
    activateMenu( mtMainMenu, true );

#ifndef REMOTE_CONTROLLER    
    setupMoves();
#endif
}

void loop() 
{
    auto currMillis = millis();

    static auto displayMillis = currMillis;
    static auto encoderMillis = currMillis;
#ifdef ENCODER_ENABLED
    static int16_t prevEncoder = 0;
    static int16_t currEncoder = 0;
#endif

    if( displayMillis + 250 < currMillis ) {
        if( displayUpdate ) {
            displayUpdate();
            drawDisplay();
        }
        displayMillis = millis();
    }

#ifdef ENCODER_ENABLED
    currEncoder += encoder.getValue();
#endif // ENCODER_ENABLED
    if( encoderMillis + 100 < currMillis ) {
        encoderMillis = currMillis;

#ifdef ENCODER_ENABLED
        if( currEncoder != prevEncoder ) {
            if( rotateAction ) 
                rotateAction( currEncoder - prevEncoder );
            currEncoder = prevEncoder;
        }
#endif // ENCODER_ENABLED

        if( clickAction ) {
#ifdef ENCODER_ENABLED
            switch( encoder.getButton() ) {
                case ClickEncoder::Clicked :       clickAction( btnSingleClick ); break;
                case ClickEncoder::DoubleClicked : clickAction( btnDoubleClick ); break;
                default: break;
            }
#endif // ENCODER_ENABLED
        }
    }

    if( movedAction and targetReached() ) {
        movedAction();
    }

    if( delayAction ) 
        delayAction();
    
    delay(10);
}

MenuTag activeMenuTag;
Menu   *activeMenu;

void activateMenu( MenuTag mt, bool reset ) // make this menu active
{
    activeMenuTag = mt;
    switch( mt ) {
        case mtMainMenu     : activeMenu = getMainMenu(); break;
        case mtPauseMenu    : activeMenu = getPauseMenu(); break;
        case mtSaveMenu     : activeMenu = getSaveLoadMenu( true ); break;
        case mtLoadMenu     : activeMenu = getSaveLoadMenu( false ); break;
    }
    if( not activeMenu ) return;
    if( reset )
        activeMenu->reset();

    rotateAction = []( int d ) {
        activeMenu->move( encoderMenuINV ? d < 0 : d > 0 );
    };
    clickAction = []( int d ) {
        if( d == btnSingleClick ) 
            activeMenu->act();
    };
    displayUpdate = [] { 
        displayData.m_state = dsMenu;
        auto &data      = displayData.m_data.m_menu;
        data.m_menu     = activeMenuTag;
        data.m_selected = activeMenu->m_selected;
    };
    
}
