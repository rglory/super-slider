#ifndef COMMANDS_HPP
#define COMMANDS_HPP

#include <Arduino.h>

struct Point {
    int16_t x = 0;
    int16_t y = 0;
};

struct Path {
    Point m_begin;
    Point m_end;
    int16_t m_speed = 100; // 10 mm/s
    uint8_t m_loops = 1;
    uint8_t m_delay = 0;
};

extern Path currentPath;

void homeX();
void homeY();
void setPointX( bool start );
void setPointY( bool start );
void gotoPos( bool start );
void setSpeedTime();
void setLoops();
void setDelay();
void executeLoop();
void saveLoadChoice( bool save );
void savePath( uint8_t num );
void loadPath( uint8_t num );

void resumeAction();

#endif // COMMANDS_HPP