#ifndef DISPLAY_HPP
#define DISPLAY_HPP

#include "settings.hpp"

enum DisplayState {
    dsNone,
    dsMenu,
    dsPosition,
    dsSpeedTime,
    dsLoops,
    dsDelay,
    dsSaveLoad,
    dsExecution,
    dsSize            // keep it last
};

struct DisplayData {
    DisplayState m_state = dsNone;
    union Data {
        bool m_corrupted;           // dsNone

        struct {
            MenuTag  m_menu;
            uint8_t  m_selected;
        } m_menu;                   // dsMenu

        struct {
            int32_t m_targetPos;
            bool    m_posX;
            bool    m_fine;
        } m_pos;                    // dsPosition

        struct {
            int16_t m_speed;
            int16_t m_dist;
            bool    m_fine;
        } m_speed;                  // dsSpeed

        struct {
            uint8_t m_loops;
        } m_loops;                  // dsLoops

        struct {
             uint8_t m_delay;
        } m_delay;                  // dsDelay

        struct {
            uint16_t m_speed;
#ifndef AXIS_X_DISABLED
            int16_t m_begX;
            int16_t m_endX;
#endif 
#ifndef AXIS_Y_DISABLED
            int16_t m_begY;
            int16_t m_endY;
#endif            
            uint8_t  m_loops;
            uint8_t  m_delay;
            bool     m_save;
        } m_saveLoad;                // dsSaveLoad

        struct {
            uint16_t m_delaying;
#ifndef AXIS_X_DISABLED
            int16_t m_targetX;
            int16_t m_currentX;
#endif 
#ifndef AXIS_Y_DISABLED
            int16_t m_targetY;
            int16_t m_currentY;
#endif            
        } m_exec;                   // dsExecution


    } m_data;
};

extern DisplayData displayData;

struct U8G2;

class DrawState {
public:
    virtual ~DrawState() = default;

    virtual void draw() = 0;
};

bool setupDisplay();
void drawDisplay();
void print_pgm( const char *s );

#endif // DISPLAY_HPP