#include "settings.hpp"
#include "strings.hpp"
#include "moves.hpp"

struct Axis {
    volatile uint8_t *m_togglePort = nullptr; // port for toggle output pin
    uint8_t           m_toggleBit  = 0;
    bool              m_flipped    = false;
    volatile int32_t  m_current    = 0;      // current coordinate in steps
    volatile int32_t  m_target     = 0;      // target  coordinate in steps
    int32_t           m_slowT      = 0;      // target  coordinate in steps to start slow down
    int32_t           m_ddelta     = 0;      // 2 * delta for Bresenham

    void setTarget( int32_t t ) 
    {
        m_target = t;
        m_ddelta = 2 * abs( m_target - m_current );
    }

    void flip( bool active )
    {
        if( m_flipped != active ) return;
        *m_togglePort = m_toggleBit; 
        m_flipped = !m_flipped;
    }
};

Axis axes[2];                    // X index 0, Y index 1

inline
Axis &axisX() { return axes[0]; }

inline
Axis &axisY() { return axes[1]; }

inline
int32_t xToSteps( int32_t x ) { return x * stepperX_SPerMM; }

inline
int16_t xFromSteps( int32_t x ) { return x / stepperX_SPerMM; }

inline
int32_t yToSteps( int32_t y ) { return y * stepperY_SPerREV / 360; }

inline
int16_t yFromSteps( int32_t y ) { return y * 360 / stepperY_SPerREV; }


int16_t  speedC          = 0;     // current speed in ticks
int16_t  speedT          = 0;     // target speed in ticks
#ifdef SLOW_MOTION
bool     onProgram       = false; // if moving from program
uint8_t  subCounter      = 0;
#endif

int32_t  currentD        = 0;     // current D for Bresenham
uint16_t counterM        = 0;     // ticks move counter
uint16_t counterS        = 0;     // ticks speed counter
bool     activeDriver    = false; // which axis is driving, true - Y, false - X
bool     passiveDir      = true;  // passive dir 
     
bool countM()
{
#ifdef SLOW_MOTION
    if( onProgram ) {
        if( ++subCounter < slowMotionDelay ) 
            return false;
        subCounter = 0;
    }
#endif
    return --counterM == 0;
}

ISR( TIMER1_COMPA_vect )
{
    for( auto &axis : axes )
        axis.flip( true );

    if( speedC and countM() ) {
        auto &active = axes[activeDriver];
        active.flip( false );
        active.m_current += speedC > 0 ? 1 : -1;

        if( active.m_current == active.m_slowT ) // check if need to start slowing
            speedT = 0;

        auto &passive = axes[!activeDriver];
        if( currentD > 0 ) {
            passive.flip( false );
            passive.m_current += passiveDir ? 1 : -1;
            currentD -= active.m_ddelta;
        }
        currentD += passive.m_ddelta;
        counterM = moveISRFreq / abs( speedC );
    }

    if( speedC != speedT and ++counterS == accelerationN ) {
        counterS = speedT ? accelerationN / 2 : 0;
        bool dir = speedT > 0;
        if( speedT ) {
            if( speedC == 0 ) {
                speedC = dir ? min( minSpeed, speedT ) : max( -minSpeed, speedT );
                digitalWrite( stepperX_DIR, ( activeDriver ? passiveDir : dir ) ^ stepperX_INV );
                digitalWrite( stepperY_DIR, ( activeDriver ? dir : passiveDir ) ^ stepperY_INV );
            } else
                speedC += speedC > speedT ? -1 : 1;
        } else {
            auto &active = axes[activeDriver];
            auto &passive = axes[!activeDriver];
            if( active.m_current == active.m_target ) {
                speedC = 0;
                passive.m_target = passive.m_current;
            } else {
                if( abs( speedC ) > minSpeed )
                    speedC += speedC > speedT ? -1 : 1;
            }
        }
    }
}


inline
uint32_t breakPath( int16_t s ) // calclulates number of steps for deceleration
{
#ifdef SLOW_MOTION    
    if( onProgram )
        return 0;
#endif    
    return abs(s) > minSpeed ? ( int32_t( s ) * s - int32_t( minSpeed ) * minSpeed ) * accelerationN / 2 / moveISRFreq : 0;
}

// Generate move values for position x mm and speed s mm/s
void moveXTo( int32_t x, uint16_t s, bool delta ) 
{
    const auto xLimitSteps = xToSteps( xLimit );

    x = xToSteps( x ) + ( delta ? axisX().m_target : 0 );
    if( x < 0 ) x = 0;
    if( x > xLimitSteps ) x = xLimitSteps;
    if( s > xSpeedLimit ) s = xSpeedLimit;

    cli();
    axisX().setTarget( x );
    axisY().m_ddelta = 0;
    const auto dir = axisX().m_current <  axisX().m_target ? 1 : -1;
    speedT = s * stepperX_SPerMM / 10 * dir;
    while( 4 * breakPath( speedT ) > uint32_t( axisX().m_ddelta ) )
        speedT /= 2;
    axisX().m_slowT = axisX().m_target - dir * breakPath( speedT );
    counterM = 1;
    activeDriver = false;
    sei();
}

// Generate move values for position y degree and speed s deg/s
void moveYTo( int32_t y, uint16_t s, bool delta ) 
{
    if( s > ySpeedLimit ) s = ySpeedLimit;
    y = yToSteps( y ) + ( delta ? axisY().m_target : 0 );

    cli();
    axisY().setTarget( y );
    axisX().m_ddelta = 0;
    const auto dir = axisY().m_current <  axisY().m_target ? 1 : -1;
    speedT = int32_t( s ) * stepperY_SPerREV / 360 * dir;
    while( 4 * breakPath( speedT ) > uint32_t( axisY().m_ddelta ) )
        speedT /= 2;
    axisY().m_slowT = axisY().m_target - dir * breakPath( speedT );
    counterM = 1;
    activeDriver = true;
    sei();
}

// Generate move values for position x mm y degree and speed s mm/s for x
void moveTo( int32_t x, int32_t y, uint16_t s, bool fromProgram ) 
{
    if( x < 0 ) x = 0;
    if( x > xLimit ) x = xLimit;
    if( s > xSpeedLimit ) s = xSpeedLimit;

    x = xToSteps( x );
    y = yToSteps( y );
    if( x == axisX().m_target and y == axisY().m_target )
        return;


    cli();
#ifdef SLOW_MOTION    
    onProgram = fromProgram;
#endif    
    axisX().setTarget( x );
    axisY().setTarget( y );
    activeDriver = axisY().m_ddelta > axisX().m_ddelta;

    auto &active   = axes[activeDriver];
    auto &passive  = axes[!activeDriver];
    passiveDir = passive.m_target > passive.m_current;
    const auto dir = ( active.m_current <  active.m_target ? 1 : -1 );
    currentD = passive.m_ddelta - passive.m_ddelta / 2;
    speedT = ( activeDriver ?  int32_t( s ) * stepperY_SPerREV / 3600 : int32_t( s ) * stepperX_SPerMM / 10 ) * dir;
    while( 4 * breakPath( speedT ) > uint32_t( activeDriver ? axisY().m_ddelta : axisX().m_ddelta ) )
        speedT /= 2;
    active.m_slowT = active.m_target - dir * breakPath( speedT );
    counterM = 1;
    sei();
}

void setupMoves()
{
    pinMode( xlimitEndstopP, INPUT_PULLUP );

    pinMode( stepperEnable, OUTPUT );
    digitalWrite( stepperEnable, 0 );
    pinMode( stepperX_DIR, OUTPUT );
    pinMode( stepperX_STEP, OUTPUT );
    digitalWrite( stepperX_STEP, 0 );
    pinMode( stepperY_DIR, OUTPUT );
    pinMode( stepperY_STEP, OUTPUT );
    digitalWrite( stepperY_STEP, 0 );

    axisX().m_togglePort = portOutputRegister( digitalPinToPort(stepperX_STEP) ) - 2;
    axisX().m_toggleBit = digitalPinToBitMask( stepperX_STEP );

    axisY().m_togglePort = portOutputRegister( digitalPinToPort(stepperY_STEP) ) - 2;
    axisY().m_toggleBit = digitalPinToBitMask( stepperY_STEP );

    counterM = moveISRFreq / 2;

    cli();
    TCCR1A = 0;
    TCCR1B = (1 << CS11) | (1 << WGM12);
    TCNT1  = 0;
    // F_CPU / ( prescaler * F) - 1
    OCR1A  = 39;
    TIMSK1 |= (1 << OCIE1A);
    sei();
}

// stop any movement
void stopMovement()
{
    if( targetReached() ) return;
    auto &active   = axes[activeDriver];
    cli();
    auto breakDist = breakPath( speedC );
    if( uint32_t( abs( active.m_target - active.m_current ) ) > breakDist ) {
        active.m_target = active.m_current + breakDist * ( speedC > 0 ? 1 : -1 );
        if( breakDist > 2 )
            active.m_slowT  = active.m_current + ( speedC > 0 ? 2 : -2 );
        else 
            speedT = 0;
    }
    else {
        speedT = 0;
    }
    sei();
}

/*void drawHomeX(  U8G2 * display )
{
    display->setDrawColor( 1 );
    display->setCursor( 4, 32 );
    display->print( strHoming );
}*/

// Home X until endpoint triggers
void homeX()
{
    //displayUpdate = drawHomeX;
    //drawDisplay();

    if( digitalRead( xlimitEndstopP ) != xlimitEndstopL ) {
        axisX().m_current = int32_t( xLimit + 10 ) * stepperX_SPerMM;
        moveXTo( 0, xSpeedHoming, false );
        while( digitalRead( xlimitEndstopP ) != xlimitEndstopL ) {
            delay( 5 );
        }
        cli();
        speedT = 0;
        zeroX();
        axisX().m_target = axisX().m_current - breakPath( xSpeedHoming );
        sei();
        delay( 200 );
    }

    moveXTo( 10, xSpeedHoming / 2 + 1, false );
    while( digitalRead( xlimitEndstopP ) == xlimitEndstopL ) {
        delay( 5 );
    }
    cli();
    speedT = 0;
    zeroX();
    sei();
    activateMenu();
}

// set current X to 0
void zeroX()
{   
    axisX().m_current = -32; // give some room to stop
    axisX().m_target  = 0;
}

// set current Y to 0
void zeroY()
{
    axisY().m_current = 0;
    axisY().m_target = 0;
}

// current X coord
int32_t currentX()
{
    return xFromSteps( axisX().m_current );
}

// target X coord
int32_t targetX()
{
    return xFromSteps( axisX().m_target );
}

// current Y coord
int32_t currentY()
{
    return yFromSteps( axisY().m_current );
}

// current Y coord
int32_t targetY()
{
    return yFromSteps( axisY().m_target );
}

bool targetReached()
{
    return axisX().m_target == axisX().m_current and axisY().m_target == axisY().m_current;
}
