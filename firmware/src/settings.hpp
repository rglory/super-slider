#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <Arduino.h>

//#define AXIS_Y_DISABLED
//#define AXIS_X_DISABLED
//#define REMOTE_CONTROLLER   // Uncomment if compile firmware for remote controller, ie no steppers
#define ENCODER_ENABLED       // Uncomment if firmware uses encoder
#define DISPLAY_ENABLED       // Uncomment if firmware uses display
//#define SLOW_MOTION           // Uncomment if firmware for slow slider

const auto slowMotionDelay = 60;

// not implemented yet
const bool smallMenuText = false;     // use text size 1 or 2 small text 6x8, large 12x16

const auto screenWidth      = 128;   // OLED display width, in pixels
const auto screenHeight     = 64;    // OLED display height, in pixels

const auto encoderCLK     = 3;       // CLK pin
const auto encoderDT      = 8;       // DT pin
const auto encoderSW      = 2;       // SW pin
const auto encoderSTEPS   = 4;       // encoder steps
const bool encoderMenuINV = false;   // reverse menu navigation direction
const bool encoderAccel   = false;   // encoder acceleration

// Movement ISR frequency
const int32_t moveISRFreq    = 50000;

// Accleleration divisor
const uint16_t accelerationN = 4;
const int16_t  minSpeed      = 128;

const int8_t maxLoops        = 10; 

// X axis settings
const auto stepperEnable   = 9;      // stepper enable pin
//const auto stepperX_SPerMM = 160;     // 200 (steps per rev) * 32 (microsteps) / ( 2 (GT2 2mm pitch) * 20 (pulley tooth count) )
const auto stepperX_SPerMM = 80;     // 200 (steps per rev) * 16 (microsteps) / ( 2 (GT2 2mm pitch) * 20 (pulley tooth count) )
const auto stepperX_INV    = false;  // change this flag if you want to referse rotation
const auto stepperX_DIR    = 4;      // DIR pin
const auto stepperX_STEP   = 5;      // STEP pin
const auto xlimitEndstopP  = 11;     // Endstop pin
const bool xlimitEndstopL  = false;  // Level of X endstop when triggered
const auto xLimit          = 1500;   // X axis size in mm
const auto xSpeedLimit     = 2000;   // X axis speed limit 0.1 mm/s
const auto xSpeedPos       = 1000;   // X axis positioning speed 0.1 mm/s
const auto xSpeedHoming    = 400;    // X homing speed 0.1 mm/s
const auto fineXmovement   = 10;     // fine movement per step for X
const auto coarseXmovement = 50;     // coarse movement per step for X

// Y asix settings
//const auto stepperY_SPerREV  = 6400;  // 200 (steps per rev) * 32 (microsteps)
const auto stepperY_SPerREV  = 12000;   // 200 (steps per rev) * 16 (microsteps) * 3.75 (gears ratio)
const auto stepperY_INV      = true;    // change this flag if you want to referse rotation
const auto stepperY_DIR      = 6;       // DIR pin
const auto stepperY_STEP     = 7;       // STEP pin
const auto ySpeedLimit       = 45;      // y axis speed limit grads/s
const auto ySpeedPos         = 30;      // y axis positioning speed grads/s
const auto fineYmovement     = 1;       // fine movement per step for Y
const auto coarseYmovement   = 10;      // coarse movement per step for Y

enum ButtonClick {
    btnSingleClick,
    btnDoubleClick
};

using DisplayUpdate = void(*)();
using EncoderAction = void(*)( int );
using MovedAction   = void(*)();
using DelayAction   = void(*)();

extern EncoderAction rotateAction;
extern EncoderAction clickAction;
extern DisplayUpdate displayUpdate;
extern MovedAction   movedAction;
extern DelayAction   delayAction;

void saveLoadPath( uint8_t num );

class Menu;

Menu *getMainMenu();
Menu *getSaveLoadMenu( bool save );
Menu *getPauseMenu();

enum MenuTag { mtMainMenu, mtSaveMenu, mtLoadMenu, mtPauseMenu };
// make this menu active, main by default
void activateMenu( MenuTag m = mtMainMenu, bool reset = false );

void drawDisplay();

#define RADIO_ENABLED  // Enabled RF24 modules
#ifdef RADIO_ENABLED

#endif // RADIO_ENABLED


#endif // SETTINGS_HPP