#include "settings.hpp"
#include "menu.hpp"

#ifdef DISPLAY_ENABLED
#include "display.hpp"

#include <U8g2lib.h>
#endif

bool Menu::act()
{ 
    auto action = m_items[m_selected].m_action; 
    if( not action ) return false; 
    action();
    return true;
}

bool Menu::prev() 
{ 
    if( m_selected > 0 ) { 
        --m_selected; 
        return true; 
    } 
    return false; 
}

bool Menu::next() 
{ 
    if( m_selected + 1 < m_size ) {
        ++m_selected;
        return true;
    }
    return false;
}

void Menu::print( U8G2 *display ) const
{
#ifdef DISPLAY_ENABLED
    if( m_header ) {
        display->setDrawColor( 1 );
        display->drawBox( 0, 0, screenWidth, 16 );
        display->setDrawColor( 0 );
        display->setCursor( 12, 12 );
        print_pgm( m_header );
    }

    display->setDrawColor( 1 );
    for( int i = -1; i < 2; ++i ) {
        auto idx = static_cast<int>( m_selected ) + i;
        if( idx < 0 or idx >= m_size ) continue;
        display->setCursor( 0, 48 + i * 16 );
        if( i == 0 ) display->print( ">> " );
        print_pgm( m_items[ idx ].m_str );
    }
#endif    
}

