#include "display.hpp"
#include "strings.hpp"
#include "menu.hpp"

DisplayData displayData;

#ifdef DISPLAY_ENABLED
#include <U8g2lib.h>
#include <avr/pgmspace.h>

inline
U8G2 *getU8G2()
{
    static U8G2_SSD1306_128X64_NONAME_2_HW_I2C display( U8G2_R0 );
    return &display;
}

bool setupDisplay()
{
    auto u8 = getU8G2();
    u8->begin();
    u8->display();
    //display.setFont( u8g2_font_9x15_mf );
    u8->setFont( u8g2_font_crox2c_tf );
    //display.enableUTF8Print();
    u8->setFontMode( 0 );
    return true;
}


class DrawNone : public DrawState {
public:
    virtual void draw() 
    {
        auto u8 = getU8G2();
        u8->setDrawColor( 1 );
        u8->drawBox( 0, 0, screenWidth, 16 );
        u8->setDrawColor( 0 );
        u8->setCursor( 12, 12 );
        print_pgm( displayData.m_data.m_corrupted ? strCorrupted : strNoConn );
    }
};

class DrawMenu : public DrawState {
public:
    virtual void draw() 
    {
        auto u8 = getU8G2();
        const auto &data = displayData.m_data.m_menu;
        Menu *m = nullptr;
        switch( data.m_menu ) {
            case mtMainMenu     : m = getMainMenu(); break;
            case mtSaveMenu     : m = getSaveLoadMenu( true ); break;
            case mtLoadMenu     : m = getSaveLoadMenu( false ); break;
            case mtPauseMenu    : m = getPauseMenu(); break;
        }
        if( not m ) return;
        m->m_selected = data.m_selected;
        m->print( u8 );
    }
};

class DrawPosition : public DrawState {
public:
    virtual void draw() 
    {
        auto u8 = getU8G2();
        const auto &data = displayData.m_data.m_pos;

        u8->setDrawColor( 1 );
        u8->drawBox( 0, 0, screenWidth, 16 );
        u8->setDrawColor( 0 );
        u8->setCursor( 12, 12 );
        print_pgm( data.m_posX ? strSetX : strSetY );
        if( data.m_fine ) print_pgm( strFine );

        u8->setDrawColor( 1 );

        u8->setCursor( 48, 48 );
        u8->print( data.m_targetPos );
    }

};

class DrawSpeedTime : public DrawState {
public:
    virtual void draw() 
    {
        auto u8 = getU8G2();
        const auto &data = displayData.m_data.m_speed;

        u8->setDrawColor( 1 );
        u8->drawBox( 0, 0, screenWidth, 16 );

        u8->setDrawColor( 0 );
        u8->setCursor( 12, 12 );
#ifndef SLOW_MOTION
        print_pgm( strSpeed );
#else
        print_pgm( strTime );
#endif
        if( data.m_fine ) print_pgm( strFine );

        u8->setDrawColor( 1 );
#ifndef SLOW_MOTION
        u8->setCursor( 48, 32 );
        u8->print( data.m_speed / 10 );
        auto rem = data.m_speed % 10;
        if( rem ) {
            u8->print( '.' );
            u8->print( rem );
        }
        u8->print( " " );
        print_pgm( strMMS );

        if( data.m_dist and data.m_speed ) {
            u8->setCursor( 48, 48 );
            u8->print( data.m_dist * 10 / data.m_speed );
            u8->print( " " );
            print_pgm( strSEC );
        }
#else
        if( data.m_dist and data.m_speed ) {
            u8->setCursor( 20, 32 );
            u8->print( 60.0 * data.m_dist * 10 / ( double( slowMotionDelay ) * data.m_speed ), 1 );
            u8->print( ' ' );
            print_pgm( strMIN );
        }
#endif        
    }
};

class DrawLoops : public DrawState {
public:
    virtual void draw() 
    {
        auto u8 = getU8G2();
        const auto &data = displayData.m_data.m_loops;

        u8->setDrawColor( 1 );
        u8->drawBox( 0, 0, screenWidth, 16 );

        u8->setDrawColor( 0 );
        u8->setCursor( 12, 12 );
        print_pgm( strLoops );

        u8->setDrawColor( 1 );
        u8->setCursor( 48, 48 );
        if( data.m_loops ) {
            u8->print( data.m_loops );
        }
        else 
            print_pgm( strINF );
    }
};

class DrawDelay : public DrawState {
public:
    virtual void draw() 
    {
        auto u8 = getU8G2();
        const auto &data = displayData.m_data.m_delay;

        u8->setDrawColor( 1 );
        u8->drawBox( 0, 0, screenWidth, 16 );

        u8->setDrawColor( 0 );
        u8->setCursor( 12, 12 );
        print_pgm( strDelay );

        u8->setDrawColor( 1 );
        u8->setCursor( 48, 48 );
        u8->print( data.m_delay );
    }
};

class DrawSaveLoad : public DrawState {
public:
    virtual void draw() 
    {
        auto u8 = getU8G2();
        const auto &data = displayData.m_data.m_saveLoad;
        u8->setDrawColor( 1 );
        u8->drawBox( 0, 0, screenWidth, 16 );

        u8->setDrawColor( 0 );
        u8->setCursor( 12, 12 );
        print_pgm(  data.m_save ? strSavedPath : strLoadedPath  );

        u8->setDrawColor( 1 );
        u8->setCursor( 2, 32 );
        print_pgm( strFrom );
#ifndef AXIS_X_DISABLED
        u8->print( data.m_begX );
#endif

#ifndef AXIS_Y_DISABLED
#ifndef AXIS_X_DISABLED
        u8->print( "/" );
#endif
        u8->print( data.m_begY );
#endif

        print_pgm( strDelaySRT );
        u8->print( data.m_delay );

        u8->setCursor( 2, 48 );
        print_pgm( strTo );
#ifndef AXIS_X_DISABLED
        u8->print( data.m_endX );
#endif

#ifndef AXIS_Y_DISABLED
#ifndef AXIS_X_DISABLED
        u8->print( "/" );
#endif
        u8->print( data.m_endY );
#endif

        u8->setCursor( 2, 64 );
        print_pgm( strSpeedSRT );
        u8->print( data.m_speed );
        print_pgm( strLoopsSRT );
        u8->print( data.m_loops );
    }
};


class DrawExecution : public DrawState {
public:
    virtual void draw() 
    {
        auto u8 = getU8G2();
        const auto &data = displayData.m_data.m_exec;

        u8->setDrawColor( 1 );
        u8->drawBox( 0, 0, screenWidth, 16 );

        u8->setDrawColor( 0 );
        u8->setCursor( 12, 12 );
        print_pgm( data.m_delaying ? strDelaying : strMoving );

        u8->setDrawColor( 1 );
        if( data.m_delaying == 0 ) {
#ifndef AXIS_X_DISABLED
            u8->setCursor( 18, 32 );
            u8->print( data.m_currentX );
#endif
#ifndef AXIS_Y_DISABLED
            u8->setCursor( 58, 32 );
            u8->print( data.m_currentY );
#endif
#ifndef AXIS_X_DISABLED
            u8->setCursor( 18, 48 );
            u8->print( data.m_targetX );
#endif
#ifndef AXIS_Y_DISABLED
            u8->setCursor( 58, 48 );
            u8->print( data.m_targetY );
#endif
        } else {
            u8->setCursor( 58, 48 );
            u8->print( data.m_delaying );
        }
    }
};

DrawNone      drawNone;
DrawMenu      drawMenu;
DrawPosition  drawPosition;
DrawSpeedTime drawSpeedTime;
DrawLoops     drawLoops;
DrawDelay     drawDelay;
DrawSaveLoad  drawSaveLoad;
DrawExecution drawExecution;

DrawState *drawHandlers[dsSize] = { 
    &drawNone,          // dsNone
    &drawMenu,          // dsMenu
    &drawPosition,      // dsPosition
    &drawSpeedTime,     // dsSpeedTime
    &drawLoops,         // dsLoops
    &drawDelay,         // dsDelay
    &drawSaveLoad,      // dsSaveLoad
    &drawExecution      // dsExecution
};

void drawDisplay()
{
    if( displayData.m_state >= dsSize ) {
        displayData.m_state = dsNone;
        displayData.m_data.m_corrupted = true;
    }
    auto u8 = getU8G2();
    u8->clearBuffer();
    u8->firstPage();
    do {
        drawHandlers[displayData.m_state]->draw();
    } while( u8->nextPage() );
}

void print_pgm( const char *s )
{
    auto u8 = getU8G2();
    while( true ) {
        char c = pgm_read_byte_near( s++ );
        if( not c ) break;
        u8->print( c );
    }
}
#else

bool setupDisplay()
{
    return true;
}

void drawDisplay()
{
}

void print_pgm( const char *s )
{
}

#endif // DISPLAY_ENABLED

